
class Triangulo:

  def __init__(self, lado1, lado2, lado3):
    self.lado1 = lado1
    self.lado2 = lado2
    self.lado3 = lado3

  @classmethod
  def novo_triangulo(cls, lado1, lado2, lado3):
    # TODO se lados não respeitarem a regra de formação de triângulos, joga exceção
    # A soma dos lados menores tem que ser maior ou igual ao lado maior (se não me engano)
    # Obs: isso seria uma coisa feia de se fazer no construtor,
    # por isso temos este método de fábrica aqui
    return cls(lado1, lado2, lado3)

  def eh_equilatero(self):
    return self.lado1 == self.lado2 and self.lado2 == self.lado3

  def eh_escaleno(self):
    return self.lado1 != self.lado2 and self.lado2 != self.lado3 and self.lado1 != self.lado3

  def eh_isosceles(self):
    return self.lado1 == self.lado2 or self.lado2 == self.lado3 or self.lado1 == self.lado3

  def area(self):
    pass

  def eh_triangulo_retangulo(self):
    pass
