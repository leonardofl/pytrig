import unittest
from unittest import TestCase
from pytrig import *

class TestTriangulo(TestCase):

  def um_triangulo_com_todos_os_lados_iguais(self):
    return Triangulo.novo_triangulo(2, 2, 2)

  def um_triangulo_com_somente_dois_lados_iguais(self):
    return Triangulo.novo_triangulo(2, 2, 3)

  def um_triangulo_com_todos_os_lados_diferentes(self):
    return Triangulo.novo_triangulo(2, 4, 3)


  def test_equilatero(self):
      self.assertTrue(self.um_triangulo_com_todos_os_lados_iguais().eh_equilatero())
      self.assertFalse(self.um_triangulo_com_somente_dois_lados_iguais().eh_equilatero())
      self.assertFalse(self.um_triangulo_com_todos_os_lados_diferentes().eh_equilatero())

  def test_isosceles(self):
      self.assertTrue(self.um_triangulo_com_somente_dois_lados_iguais().eh_isosceles())
      self.assertTrue(self.um_triangulo_com_todos_os_lados_iguais().eh_isosceles()) #!?
      self.assertFalse(self.um_triangulo_com_todos_os_lados_diferentes().eh_isosceles())

  def test_escaleno(self):
      self.assertTrue(self.um_triangulo_com_todos_os_lados_diferentes().eh_escaleno())
      self.assertFalse(self.um_triangulo_com_somente_dois_lados_iguais().eh_escaleno())
      self.assertFalse(self.um_triangulo_com_todos_os_lados_iguais().eh_escaleno())

if __name__ == '__main__':
    unittest.main()
